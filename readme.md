## Setup

Install ruby

```
gem install jekyll bundler
```


Browse to localhost:4000

## Deploy

set up ssh:
generate a key: ssh-keygen
copy public key out: ssh-copy-id toastuser@laurelwood.dreamhost.com

### to copy to test:

from root dir (eg ~/dev/toast-mm)
```
bundle exec jekyll build --config _config.yml,_config.test.yml
```
```
rsync --exclude=robots.txt --perms --recursive --verbose --compress --delete --chmod=Du=rwx,Dgo=rx,Fu=rw,Fgo=r _site/ toastuser@laurelwood.dreamhost.com:~/test.toastcoffeewinebar.com
```

### To Prod
```
bundle exec jekyll build --config _config.yml
```
```
rsync --perms --recursive --verbose --compress --delete --chmod=Du=rwx,Dgo=rx,Fu=rw,Fgo=r _site/ toastuser@laurelwood.dreamhost.com:~/toastcoffeewinebar.com
```

