---
layout: splash
permalink: /
header:
  overlay_color: "#5e616c"
  overlay_image: /assets/images/mainwine.jpg
  caption:
excerpt: 'toast, taste, talk'
title: '&nbsp;'
feature_row:
  - image_path: /assets/images/portafilter.jpg
    alt: "The Coffee (and Not Coffee)"
    url: "/coffee"
    btn_class: "btn"
    btn_label: "The Coffee (& Not Coffee)"
  - image_path: /assets/images/small-toast-coffee.jpg
    alt: "Food"
    url: "/food"
    btn_class: "btn"
    btn_label: "The Food"
  - image_path: /assets/images/wineglass.jpg
    alt: "Wine and Beer"
    url: "/wine-and-beer"
    btn_class: "btn"
    btn_label: "The Wine & Beer"
---

<center>
Toast is proud to announce our <a href="/assets/Toast Wine Club.pdf">Joy of Wine</a> and <a href="/assets/Toast Coffee Club.pdf">Joy of Coffee</a> subscriptions! <br /><br />
<a href = "https://squareup.com/gift/VK5PG7760A99J/order">Purchase E-Gift Cards Here!</a>
<br />
<br />
</center>
{% include feature_row id="intro" type="center" %}

{% include feature_row %}
