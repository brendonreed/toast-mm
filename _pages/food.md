---
permalink: /food/

---

## TOAST
Honey Butter Toast (2 pieces) {4.25}
: 

Cream Cheese & Fig Jam Toast (2 pieces) {4.75}
: 

Cinnamon & Sugar Toast (2 pieces) {4.75}
: 

## BAGELS
Make Your Own Bagel {3.00}
Your choice of an everything or regular bagel paired with the following options: 
: Cream Cheese
: Butter
: Hummus
: Peanut Butter & Jelly (Strawberry or Blueberry) 
: Nutella
: Mozzarella, Pesto, and Tomato {additional 2.00}
: Ham, Egg, and Cheddar Cheese {additional 2.50}
: Sausage, Egg, and Cheddar Cheese {additional 2.50}

## THE REST
Assorted Muffins {3.5}
: 

Coconut Macaroons {2.75}
: 

Oatmeal {4.99}
: Served with milk and your choice of brown sugar, pecans, raisins, or coconut shavings

Waffles {6.99}
: Your choice of butter maple syrup or chocolate chip waffles (2) served with butter and syrup

Breakfast Panini {6.99}
: Sourdough bread, cheddar cheese, ham, & egg

Bubba’s Banana Chips or Snack Pack     {2.50}  
: 

Everything Beef Jerky     {4.00}  
: 

<br />
<br />

#### The following paninis are served with your choice of potato salad or chips.  Add ham to any panini for $1.50

Three Cheese Panini {10.99}
: Garlic Parmesan bread, provolone, mozzarella, & basil pesto
: Pairs well with the Muirwood Chardonnay or the Trivento Malbec 

Mediterranean Panini     {11.99}
: Garlic Parmesan bread, provolone, mozzarella, oven roasted tomatoes, spinach, & basil pesto
: Pairs well with the Muirwood Chardonnay or the Trivento Malbec 

Reuben Panini {13.99}
: Marble rye bread, corned beef, sauerkraut, swiss cheese, & thousand-island
: Pairs well with the Provincia di Pavio Moscato or the Rickshaw Cabernet Sauvignon

Oven Roasted Tomato, Hummus, and Spinach Panini {10.99}
: Sourdough bread, roasted tomatoes, hummus, & baby spinach
: Pairs well with The Pinot Project Rosé or the Francis Ford Coppola Pinot Noir

Charcuterie Board (individual size) {9.99}
: Cheeses, meats, crackers, & jam
: Pairs well with the Guntrum Riesling or the Francis Ford Coppola Pinot Noir

Charcuterie Board (to share) {19.99}
: Cheeses, meats, hummus, olives, peppercinis, nuts, crackers, & jam
: Pairs well with the Guntrum Riesling or the Francis Ford Coppola Pinot Noir

Hummus & Pita Bread {3.99}
: 

Mediterranean Pita Melt {5.99}
: Toasted and open-faced, topped with olive oil, mozzarella, olives, and tomatoes

Toasted S’mores (2) {3.25}
: 


## FOR KIDS
### Crustless Hazelnut Sandwich {2.50}
### PB&J Sandwich {3.99}
### Grilled Cheese Panini {4.99}
### Mermaid Toast {1.99}


&nbsp;

: \*Gluten free bread available, but prepared in the same areas as the bread with gluten.
: \*All bread is vegan with the exception of the brioche &nbsp;





