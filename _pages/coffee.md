---
sidebar: 
  nav: "menu"
permalink: /coffee/

---

# The Coffee (& Not Coffee)

Coffee
: Drip  {2.5}
: Cold brew 12/16 oz  {3.5/4.5}
: Pour over  {3.25}
: Espresso (2 shots)  {2.65}
: Macchiato {3.25}
: Cappuccino {3.75}
: Red Eye  {3.50}
: 96 oz to go carafe  {15}
: Latte 12/16 oz  {4.10/5.45}
: Flavored latte 12/16 oz  {4.25/5.65}
: Americano 12/16 oz  {2.75/3}
: Mocha 12/16 oz  {4.60/5.95}
: Dirty Chai  12/16 oz  {4.25/5.65}

Not Coffee
: Steamer 12/16 oz  {3.5/4.5}
: Hot chocolate 12 oz  {3.5}
: Chocolate Milk 12 oz  {3.5}
: Hot tea  {2.75}
: London Fog  {3.75}
: Kombucha  {4}
: Italian soda 12/16 oz  {3.75/4.75}
: Black Iced tea 12/16 oz  {2.5/3.5}
: Thai Tea 12/16 oz  {4.75/5.1}
: Matcha Latte 12/16 oz  {4.25/5.65}

Extras
: Extra tea bag {.75}
: Extra shot {1}
: Extra syrup {.5}
: Soy milk substitute {.75}
: Almond milk substitute {.75}
: Coconut milk substitute {.75}
: Oat milk substitute {.75}

Syrups
: Chai
: Vanilla
: Almond
: Coconut
: Caramel
: Hazelnut
: Pistachio
: Lavender
: Raspberry
: Strawberry
: Irish Cream
: Peppermint
: SF Vanilla
: SF Chocolate
: SF Caramel
: SF Strawberry

Sauces
: Dark chocolate
: Caramel
: White Chocolate
: Sugar Free Dark Chocolate
