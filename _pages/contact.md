---
title: We'd love to hear from you!
permalink: /contact/

---


To inquire about reserving Toast, ask about specials, or just provide feedback, please contact us using one of the methods below.  

{{ site.address }}

{{ site.phone }}

<{{ site.email }}>

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3034.8024158529274!2d-104.9041125842297!3d40.47963597935783!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x876eb1d04769c845%3A0x2979253b74051816!2s205+4th+St%2C+Windsor%2C+CO+80550!5e0!3m2!1sen!2sus!4v1496326054594" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>

<p />
Sign up for our newsletter to keep up to date on events and specials at Toast!  
<form action="https://squareup.com/outreach/XiwuSx/subscribe" method="POST" target="_blank"><input type="email" name="email_address" placeholder="Your Email Address" style="height: 38px; padding: 0 16px; font-size: 14px; border: 1px solid #bec3c8; border-radius: 3px; margin-right: 8px; font-family: 'Helvetica Neue', Helvetica, sans-serif;"><button type="submit" style="cursor: pointer; background-color: #ed9356; color: white; height: 40px; border: 0; border-radius: 3px; font-size: 14px; padding: 0 16px; font-family: 'Helvetica Neue', Helvetica, sans-serif; font-weight: 500;">Join Now</button></form>


