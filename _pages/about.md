---
layout: single
title: About Us
permalink: /about/

---
Toast is located in Downtown Windsor, Colorado. We offer freshly roasted coffee from nationally recognized and locally owned Novo Coffee, a variety of wines and beers, and freshly prepared food options while providing exceptional customer service. 
		
It’s our goal to provide a relaxing and unique environment to hang out with friends, read a book, or meet with colleagues. 

<!-- SnapWidget 
<script src="https://snapwidget.com/js/snapwidget.js"></script>
<iframe src="https://snapwidget.com/embed/452812" class="snapwidget-widget" allowTransparency="true" frameborder="0" scrolling="no" style="border:none; overflow:hidden; width:100%; "></iframe>
-->