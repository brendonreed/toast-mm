---
layout: single
title: 
permalink: /events/

---

## Upcoming Events

<iframe src="https://calendar.google.com/calendar/b/2/embed?height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=toastcoffeewinebar.com_4t3jcbffjpvl5aj0joneofn5vc%40group.calendar.google.com&amp;color=%235F6B02&amp;ctz=America%2FDenver" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>


## Every day Happy Hour
3:00 pm—7:00 pm Tuesday-Friday, All day Saturday
$4 house red and white
$1 off all by the glass wines
$4 rotating beer on tap 
 
## Wednesday
Wine Wednesday! 50% off wine bottles!  (Does not include the By the Bottle Wine selection)

## Saturday
All you can drink mimosas $10 until noon

## All Day Every Day
Wine Flights! 4 wines of your choice so you can try more of what we have to offer! $20-24


## Host Your Event Here

Toast is an intimate space located in the heart of Downtown Windsor, Colorado.

It's an ideal space for holiday parties, birthdays, fundraisers, and more.
We can host groups of up to 35 people.

Email us at <{{ site.email }}>
