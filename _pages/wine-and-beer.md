---
permalink: /wine-and-beer/

---
# WINE BY THE GLASS

## SPARKLING &nbsp;
Provincia di Pavie, Moscato (Split) $9 
: 2018 / Italy

## ROSE &nbsp;
 
The Pinot Project, Rosé $9 / $33
: 2018 / France


## WHITE &nbsp;

House White $6
: 
<br/>

Guntrum, Riesling $8 / $29
: 2018 / Germany
<br/><br/>

Muirwood, Chardonnay $9 / $33
: 2017 / California
<br/><br/>

The Hess Collection, Shirtail Ranches, Sauvignon Blanc $8 / $29
: 2017 / California


## RED &nbsp;
 
House Red $6
: 
<br/>

Francis Ford Coppola, Pinot Noir $11 / $41
: 2018 / Oregon
<br/><br/>

Trivento Reserve, Malbec $7 / $25
: 2018 / Argentina
<br/><br/>

Richshaw, Cabernet Sauvignon $9 / $33
: 2017 / California
<br/><br/>

Locations, Red Blend $13 / $49
: 2017 / California


<br/><br/>
# WINE BY THE BOTTLE

## SPARKLING &nbsp;

J Vineyards & Winery, Brut Rosé $49 
: N.V. / California, Russian River Valley


## WHITE &nbsp;

Anne Amie, Pinot Blanc $45
: 2016 / Oregon, Willamette Valley 
<br/><br/>

Kelleher, Sauvignon Blanc $64
: 2017 / California, Napa Valley
<br/><br/>

David Arther, Chardonnay $111
: 2013 / California, Napa Valley


## RED &nbsp;

Croix, Pinot Noir $98
: 2017 / California, Russian River Valley
<br/><br/>

Corte Fornaledo, Valpolicella Ripasso $69
: 2014 / Italy
<br/><br/>

Twomey Cellars by Oak Silver, Merlot $98
: 2017 / California, Napa Valley
<br/><br/>

Stag’s Leap Wine Cellars, Cabernet Sauvignon $92
: 2017 / California, Napa Valley
<br/><br/>

Silencieux, Cabernet Sauvignon $65
: 2017 / California, Napa Valley
<br/><br/>

Chateauneuf-du-Pape, Red Blend $111
: 2016 / France
<br/><br/>

Orin Swift Abstract, Red Blend $69
: 2018 / California


<br/><br/>
# LIMITED SUPPLY BY THE BOTTLE WINES &nbsp;

## SPARKLING &nbsp;

Chandon Brut $52
: 


## ROSE &nbsp;

Belleruche Rose $29
: 
<br/>


Torres, Sangre De Toro Catalunya Rosé $29
: 


## WHITE &nbsp;

Sant Anna Tenuta Pinot Grigio Bottle $29
: 
<br/>


Mirassou Moscato Bottle $22
: 
<br/>


Dr. L Riesling $33
: 
<br/>


Erath Pinot Gris $37
: 
<br/>


Michael David Sauvignon Blanc $37
: 
<br/>


Gruner Veltliner Glatzer $39
: 
<br/>


Chateau St. Michelle, Gewurztraminer $23
: 
<br/>


Freakshow Chardonnay $37
: 
<br/>


Maison Ambroise Bourgogne Aligote $49
: 
<br/>


Loscano Chardonnay $41
: 
<br/>


Tenuta Ca'Bolani Pinot Grigio $37
: 
<br/>


Outcast Chardonnay $89
: 
<br/>


Martin Codax Albarino $37
: 


## RED &nbsp;

Beckon Pinot Noir $41
: 
<br/>


Bonterra Zinfandel Bottle $33
: 
<br/>


Canon 13, Pinot Noir $45
: 
<br/>


Chateau Souverain Cab Bottle $25
: 
<br/>


Columbia Cabernet Sauvignon $37
: 
<br/>


Ercavio, Tempranillo Roble $33
: 
<br/>


The Path, Merlot $25
: 
<br/>


Sean Minor, Cabernet Sauvignon $57
: 
<br/>


Intrinsic Cab Sauv $45
: 
<br/>


Freakshow Red Blend $37
: 
<br/>


Torres "Ibericos" Crianza Tempranillo $33
: 
<br/>


Freakshow Petit Petit $37
: 
<br/>


BV Merlot $53
: 
<br/>


19 Crimes Sauvignon Block $37
: 
<br/>


Loscano Cabernet Sauvignon $41
: 
<br/>


Outcast Mindset $85
: 
<br/>


Outcast OML $56
: 
<br/>


Outcast The Drifter $85
: 
<br/>


Palacios Remondo, Rioja La Montesa Grenach $57
: 
<br/>


Fontanabianca Red Blend $59
: 


<br/><br/>
# BEER &nbsp;

Current beer selections available:
: Avery Stampede
: Good River American Pilsner
: Denver Brewing Princess Yum Yum Raspberry Kolsch
: Dry Dock Sour Apricot
: Denver Brewing Juicy Freak IPA 
: Great Divide Titan IPA
: Dry Dock Hazy IPA 
: Avery Ellies Brown Ale
: Saw Tooth Ale
: Avery Brewing Cherry Lime Hard Seltzer
: Ska Hibiscus Lime Hard Seltzer



